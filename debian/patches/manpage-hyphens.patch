Fix unescaped hyphens in manpage
--- a/doc/cwdaemon.8.in
+++ b/doc/cwdaemon.8.in
@@ -20,7 +20,7 @@
 between parts of escaped requests.
 
 Some of the arguments and requests require passing a value (e.g. Morse
-speed [wpm], tone (frequency) [Hz] etc.). Call "cwdaemon -h" to see
+speed [wpm], tone (frequency) [Hz] etc.). Call "cwdaemon \-h" to see
 default values for these arguments/requests, and ranges of accepted
 values.
 
@@ -44,7 +44,7 @@
 .TP
 \fBPrint help text to stdout and exit\fR
 .IP
-Command line argument: -h, --help
+Command line argument: \-h, \-\-help
 
 .IP
 Escaped request: N/A
@@ -54,7 +54,7 @@
 .TP
 \fBPrint version information to stdout and exit\fR
 .IP
-Command line argument: -V, --version
+Command line argument: \-V, \-\-version
 
 .IP
 Escaped request: N/A
@@ -64,7 +64,7 @@
 .TP
 \fBSet hardware keying device\fR
 .IP
-Command line argument: -d, --cwdevice <device>
+Command line argument: \-d, \-\-cwdevice <device>
 
 .IP
 Escaped request: <ESC>8<device>
@@ -77,7 +77,7 @@
 .TP
 \fBDon't fork daemon, run in foreground\fR
 .IP
-Command line argument: -n, --nofork
+Command line argument: \-n, \-\-nofork
 
 .IP
 Escaped request: N/A
@@ -90,7 +90,7 @@
 .TP
 \fBSet network UDP port\fR
 .IP
-Command line argument: -p, --port <port number>
+Command line argument: \-p, \-\-port <port number>
 
 .IP
 Escaped request: <ESC>9<port number>
@@ -103,7 +103,7 @@
 .TP
 \fBSet process priority (niceness)\fR
 .IP
-Command line argument: -P, --priority <priority>
+Command line argument: \-P, \-\-priority <priority>
 
 .IP
 Escaped request: N/A
@@ -113,7 +113,7 @@
 .TP
 \fBSet Morse speed [wpm]\fR
 .IP
-Command line argument: -s, --wpm <speed>
+Command line argument: \-s, \-\-wpm <speed>
 
 .IP
 Escaped request: <ESC>2<speed>
@@ -123,7 +123,7 @@
 .TP
 \fBSet PTT delay [ms] (TOD, Turn On Delay)\fR
 .IP
-Command line argument: -t, --pttdelay <delay>
+Command line argument: \-t, \-\-pttdelay <delay>
 
 .IP
 Escaped request: <ESC>d<delay>
@@ -146,7 +146,7 @@
 .TP
 \fBSet sound system (sound backend)\fR
 .IP
-Command line argument: -x, --system <system>
+Command line argument: \-x, \-\-system <system>
 
 .IP
 Escaped request: <ESC>f<system>
@@ -159,7 +159,7 @@
 .TP
 \fBSet sound volume for soundcard [%]\fR
 .IP
-Command line argument: -v, --volume <volume>
+Command line argument: \-v, \-\-volume <volume>
 
 .IP
 Escaped request: <ESC>g<volume>
@@ -169,7 +169,7 @@
 .TP
 \fBSet Morse weighting\fR
 .IP
-Command line argument: -w, --weighting <weighting>
+Command line argument: \-w, \-\-weighting <weighting>
 
 .IP
 Escaped request: <ESC>7<weighting>
@@ -179,7 +179,7 @@
 .TP
 \fBSet tone (frequency) of sound [Hz]\fR
 .IP
-Command line argument: -T, --tone <tone>
+Command line argument: \-T, \-\-tone <tone>
 
 .IP
 Escaped request: <ESC>3<tone>
@@ -191,14 +191,14 @@
 .TP
 \fBIncrease verbosity of debug output\fR
 .IP
-Command line argument: -i
+Command line argument: \-i
 
 .IP
 Escaped request: N/A
 
 .IP
-The argument can be repeated up to four times (-iiii) to gradually
-increase the verbosity.  Alternatively you can use -y/--verbosity
+The argument can be repeated up to four times (\-iiii) to gradually
+increase the verbosity.  Alternatively you can use \-y/\-\-verbosity
 option.
 
 
@@ -206,13 +206,13 @@
 .TP
 \fBSet verbosity threshold for debug strings\fR
 .IP
-Command line argument: -y, --verbosity <threshold>
+Command line argument: \-y, \-\-verbosity <threshold>
 
 .IP
 Escaped request: N/A
 
 .IP
-Alternatively you can use -i option.
+Alternatively you can use \-i option.
 
 .IP
 See chapter "DEBUGGING" below for more information.
@@ -224,7 +224,7 @@
 .TP
 \fBSet numeric value of libcw debug flags\fR
 .IP
-Command line argument: -I, --libcwflags <flags>
+Command line argument: \-I, \-\-libcwflags <flags>
 
 .IP
 Escaped request: N/A
@@ -234,7 +234,7 @@
 .TP
 \fBSet debug output\fR
 .IP
-Command line argument: -f, --debugfile <output>
+Command line argument: \-f, \-\-debugfile <output>
 
 .IP
 Escaped request: N/A
@@ -376,7 +376,7 @@
 versions of cwdaemon. Don't depend on current setup.
 
 The verbosity threshold can be specified through command line
-arguments: -i, -y, or --verbosity. -y and --verbosity accept
+arguments: \-i, \-y, or \-\-verbosity. \-y and \-\-verbosity accept
 "threshold" value (single character) as specified below:
 
 .TP
@@ -399,8 +399,8 @@
 Debug strings can be printed to stdout, stderr, or disc file. Printing
 to stdout or stderr is available only if cwdaemon has not forked.
 Printing to disc file is available regardless of whether cwdaemon has
-forked or not.  You can specify intended debug output using -f or
---debugfile command line argument. Values "stdout" and "stderr" passed
+forked or not.  You can specify intended debug output using \-f or
+\-\-debugfile command line argument. Values "stdout" and "stderr" passed
 to cwdaemon with these arguments are recognized as special file
 names. "syslog" value is also treated as special value, but is not
 supported at the moment. Every other value will be treated as path to
@@ -409,7 +409,7 @@
 line arguments, but you can.
 
 .P
-Regardless of values passed through -f or --debugfile arguments,
+Regardless of values passed through \-f or \-\-debugfile arguments,
 cwdaemon opens a syslog file and (when forked) prints some (few)
 messages to the syslog. There is no way to override this at the
 moment. Which messages are always printed to syslog, and which can be
